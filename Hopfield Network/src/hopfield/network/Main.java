package hopfield.network;

/**
 *
 * @author Umberto
 */
public class Main {

    private static int N_TRAINSET = 6;
    private static int N_TESTSET = 6;

    private static int NUMBER_OF_NEURONS = 64;

    private static int[][] input
            = {{0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0, // ‘A’
                0, 1, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1
            },
               {1, 1, 1, 1, 1, 1, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,// ‘B’
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0},
               {0, 0, 1, 1, 1, 1, 1, 1, // ‘C’
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1, 1},
               {1, 0, 0, 0, 0, 0, 0, 1,
                0, 1, 0, 0, 0, 0, 1, 0, // ‘X’
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 1, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,},
               {0, 1, 0, 0, 0, 0, 0, 1,
                0, 0, 1, 0, 0, 0, 1, 0, // ‘Y’
                0, 0, 0, 1, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,},
               {1, 1, 1, 1, 1, 1, 1, 1,// ‘Z’
                0, 0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1}
            };

    private static int NUMBER_OF_EXECUTIONS = 500;
    private static int NUMBER_OF_NOISE_BITS = 1;

    public static void main(String[] args) {
        HopfieldLayer net = new HopfieldLayer(NUMBER_OF_NEURONS);

        net.setWeights(input, N_TRAINSET);

        int[][] output = net.train(input, N_TRAINSET);

        float rateTest = net.test(input, N_TESTSET, output);
        System.out.printf("Success rate for test %.2f%%\n", rateTest);

        //robustness test for bits from 1 to 15
        for (int i = 1; i < 16; i++) {
            net.
                    robustnessTest(input, N_TRAINSET, output, i, NUMBER_OF_EXECUTIONS);
        }

        //speed statistics
        gatherStats(net);
    }

    private static void gatherStats(HopfieldLayer net) {
        int NUMB_OF_RUNS = 10000;
        long sum = 0;
        for (int i = 0; i < NUMB_OF_RUNS; i++) {
            long startTime = System.currentTimeMillis();
            net.setWeights(input, N_TRAINSET);
            net.train(input, N_TRAINSET);
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;
            sum += totalTime;
        }
        System.out.
                printf("Execution time for %d trainings is: %f milliseconds\n", NUMB_OF_RUNS, (float) sum / NUMB_OF_RUNS);
    }

}
