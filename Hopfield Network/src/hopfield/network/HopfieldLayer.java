/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hopfield.network;

import java.util.Arrays;

/**
 *
 * @author Umberto
 */
public class HopfieldLayer {

    int[] inputNeuron;
    int[] output;
    int[] w; //weigth matrix

    public HopfieldLayer(int numberOfNeurons) {
        inputNeuron = new int[numberOfNeurons]; //create layer
        output = new int[numberOfNeurons]; //create layer

    }

    public void setWeights(int[][] input, int numberOfInputs) {
        int size = input[0].length;
        w = new int[size * size];
        //the vector is already initialize to '0'
        int[][] bipolarInput = binaryToBipolar(input, numberOfInputs);

        for (int k = 0; k < numberOfInputs; k++) {
            //for each vector to learn

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    //weigth is 0 if on diagonal otherwise is the product of the two neurons value
                    w[i * size + j] += (i != j) ? bipolarInput[k][i] * bipolarInput[k][j] : 0;
                }
            }
        }
    }

    public int[][] train(int[][] input, int numberOfInputs) {
        int threshold = 0;
        int[][] finalOutputs = new int[numberOfInputs][input[0].length];

        int[][] bipolarInput = binaryToBipolar(input, numberOfInputs);

        //until convergence   
        //inputNeurons now is equal to the pattern to learn
        for (int k = 0; k < numberOfInputs; k++) {
            inputNeuron = this.extractRow(bipolarInput, k);
            converge(threshold);
           // System.out.println("Converged!");

            System.arraycopy(output, 0, finalOutputs[k], 0, output.length);
            //prettyPrint(finalOutputs, k);
        }

        return finalOutputs;
    }

    private void prettyPrint(int[][] finalOutputs, int k) {
        for (int i = 0; i < finalOutputs[k].length; i++) {
            System.out.printf("[%2d]", (finalOutputs[k][i] == -1) ? 0 : 1);
            if ((i + 1) % 8 == 0) {
                System.out.println("");
            }
        }
        System.out.println("\n");
    }

    private void converge(int threshold) {
        boolean changed = true;
        while (changed) {
            changed = false;
            //from the first neuron to the last
            //update neurons
            int bit = 0;

            for (int i = inputNeuron.length - 1; i >= 0; i--) {
                output[i] = 0;
                for (int j = 0; j < inputNeuron.length; j++) {                    
                    output[i] += inputNeuron[j] * w[i * inputNeuron.length + j];
                }

                //activation
                output[i] = (output[i] > threshold) ? 1
                        : (output[i] == threshold) ? inputNeuron[i] : -1;

                //update change, once a neuron is changed net keeps updating
                changed = changed || inputNeuron[i] != output[i];
                if (inputNeuron[i] != output[i]) {
                    bit++;
                }
                //reset input
                inputNeuron[i] = output[i];
            }           
        }
    }

    public float test(int[][] input, int numberOfInputs, int[][] givenOutput) {
        int threshold = 0;
        int success = 0;
        int[][] bipolarInput = binaryToBipolar(input, numberOfInputs);

        //until convergence   
        //inputNeurons now is equal to the pattern to learn
        for (int k = 0; k < numberOfInputs; k++) {
            inputNeuron = this.extractRow(bipolarInput, k);
            converge(threshold);

            //if the two array are equal increment success           
            success += (Arrays.equals(this.output, extractRow(givenOutput, k))) ? 1 : 0;
        }
        float rate = (float) success / numberOfInputs * 100;
        return rate;
    }

    private void insertNoise(int[] input, int numberOfNoiseBits) {
        int index;
        //for each noise bit to itroduce
        for (int i = 0; i < numberOfNoiseBits; i++) {
            //select one random bit in the vector and reverse it
            index = RandomGenerator.randomInRange(0, input.length - 1);
            input[index] = (input[index] == -1) ? 1 : -1; //reverse the bit
        }
    }

    public void robustnessTest(int[][] input, int numberOfInputs, int[][] givenOutput, int numberOfNoiseBits, int numberOfExecutions) {
        int threshold = 0;
        System.out.printf("Noise bits: %d\n",numberOfNoiseBits);
        int[][] bipolarInput = binaryToBipolar(input, numberOfInputs);

        for (int k = 0; k < numberOfInputs; k++) {
            int success = 0;
            for (int count = 0; count < numberOfExecutions; count++) {
                inputNeuron = this.extractRow(bipolarInput, k);
                insertNoise(inputNeuron, numberOfNoiseBits);
                converge(threshold);

                //if the two array are equal increment success           
                success += (Arrays.
                        equals(this.output, extractRow(givenOutput, k))) ? 1 : 0;
            }
            float rate = (float) success / numberOfExecutions;
            System.out.
                    printf("Letter %d, success rate: %.2f%%\n", k, rate*100);
        }
    }

    //**UTILITY FUNCTIONS **//
    private int[] extractRow(int[][] dataset, int row) {
        //System.out.println("Extracting row...\n");
        int[] tmpArray = new int[dataset[row].length];
        System.arraycopy(dataset[row], 0, tmpArray, 0, dataset[row].length); //System.out.printf("%f|",tmpArray[i]);
        return tmpArray;
    }

    private int[][] binaryToBipolar(int[][] input, int numberOfInputs) {
        //for each input vector
        int size = input[0].length;
        int[][] tmp = new int[numberOfInputs][size];

        for (int k = 0; k < numberOfInputs; k++) {
            for (int i = 0; i < size; i++) {
                tmp[k][i] = (input[k][i] == 0) ? -1 : 1;
            }
        }
        return tmp;
    }

    private void printW() {
        System.out.printf("Total number of weigths is %d\n", w.length);
        for (int i = 0; i < w.length; i++) {
            System.out.printf("[%d] ", w[i]);
            if ((i + 1) % 64 == 0) {
                System.out.println("");
            }
        }
    }

    private void printInput(int[] input) {
        for (int i = 0; i < input.length; i++) {
            System.out.printf("[%d]", input[i]);
        }
    }
}
